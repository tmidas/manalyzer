// manalyzer.h

#ifndef MANALYZER_H
#define MANALYZER_H

#include <string>
#include <vector>
#include <deque>

#include <thread>
#include <mutex>
#include <atomic>

#include "midasio.h"
#include "mvodb.h"

class TARootHelper;
class TAMultithreadHelper;
class TAFlowEvent;

class TARunInfo
{
 public:
   int fRunNo = 0;
   std::string fFileName;
   MVOdb* fOdb = NULL;
   TARootHelper* fRoot = NULL;
   TAMultithreadHelper* fMtInfo = NULL;
   std::vector<std::string> fArgs;
   static std::vector<std::string> fgFileList;
   static int fgCurrentFileIndex; // this global variable is initialized in manalyzer.cxx
   
 public:
   TARunInfo(int runno, const char* filename, const std::vector<std::string>& args);
   ~TARunInfo();

 public:
   void AddToFlowQueue(TAFlowEvent*);
   TAFlowEvent* ReadFlowQueue();

 private:
   TARunInfo() {}; // hidden default constructor

 private:
   std::deque<TAFlowEvent*> fFlowQueue;
};

class TAFlowEvent
{
 public:
   TAFlowEvent* fNext = NULL;

 public:
   TAFlowEvent(TAFlowEvent*);
   virtual ~TAFlowEvent();

   template<class T> T* Find()
      {
         TAFlowEvent* f = this;
         while (f) {
            T *ptr = dynamic_cast<T*>(f);
            if (ptr) return ptr;
            f = f->fNext;
         }
         return NULL;
      }

 private:
   TAFlowEvent() {}; // hidden default constructor 
};

typedef int TAFlags;

#define TAFlag_OK          0
#define TAFlag_SKIP    (1<<0)
#define TAFlag_QUIT    (1<<1)
#define TAFlag_WRITE   (1<<2)
#define TAFlag_DISPLAY (1<<3)
#define TAFlag_SKIP_PROFILE (1<<4)

class TARunObject
{
 public:
   std::string fModuleName;

 public:
   TARunObject(TARunInfo* runinfo); // ctor
   virtual ~TARunObject() {}; // dtor

 public:
   virtual void BeginRun(TARunInfo* runinfo); // begin of run
   virtual void EndRun(TARunInfo* runinfo); // end of run
   virtual void NextSubrun(TARunInfo* runinfo); // next subrun file

   virtual void PauseRun(TARunInfo* runinfo); // pause of run (if online)
   virtual void ResumeRun(TARunInfo* runinfo); // resume of run (if online)

   virtual void PreEndRun(TARunInfo* runinfo); // generate flow events before end of run

   virtual TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow);
   virtual TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow);
   virtual void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event);

 private:
   TARunObject(); // hidden default constructor
};

class TAFactory
{
 public:
   TAFactory() {}; // ctor
   virtual ~TAFactory() {}; // dtor

 public:
   virtual TARunObject* NewRunObject(TARunInfo* runinfo) = 0; // factory for Run objects

 public:
   virtual void Usage(); // Display usage (flags to pass to init etc)
   virtual void Init(const std::vector<std::string> &args); // start of analysis
   virtual void Finish(); // end of analysis
};

template<class T> class TAFactoryTemplate: public TAFactory
{
   T* NewRunObject(TARunInfo* runinfo)
   {
      return new T(runinfo);
   }
};

class TARegister
{
 public:
   TARegister(TAFactory* m);
   //static void Register(TAModuleInterface* m);
   //static std::vector<TAModuleInterface*>* fgModules;
   //static std::vector<TAModuleInterface*>* Get() const;
};

#ifdef HAVE_ROOT

#include "TFile.h"
#include "TDirectory.h"
#include "TApplication.h"

class XmlServer;
class THttpServer;

class TARootHelper
{
 public:
   static std::string fOutputDirectory;
   static std::string fOutputFileName;
   TFile* fOutputFile = NULL;
   static TDirectory*   fgDir;
   static TApplication* fgApp;
   static THttpServer*  fgHttpServer;

 public:
   TARootHelper(const TARunInfo*);
   ~TARootHelper(); // dtor

 private:
   TARootHelper() { }; // hidden default constructor
};
#endif

typedef std::deque<TAFlowEvent*> TAFlowEventQueue;
typedef std::deque<TAFlags*>     TAFlagsQueue;

class TAMultithreadHelper
{
public: // per-module queues and threads
   std::vector<std::mutex>       fMtFlowQueueMutex; // queue lock
   std::vector<TAFlowEventQueue> fMtFlowQueue;      // event queue
   std::vector<TAFlagsQueue>     fMtFlagQueue;      // flags queue
   std::vector<std::thread*>     fMtThreads;        // threads
   std::vector<std::atomic<bool>>             fMtThreadIsRunning;  // "thread is running" flag
   std::vector<std::atomic<bool>>             fMtThreadIsBusy;     // "thread is analyzing an event" flag

public: // shutdown and quit control   
   std::atomic<bool> fMtShutdownRequested; // flag to shutdown all threads
   std::atomic<bool> fMtQuitRequested; // flag TAFlag_QUIT from per-module threads

public: // queue settings
   int  fMtQueueDepth = 0; // maximum number of flow events to queue
   int  fMtQueueFullUSleepTime = 0;  // u seconds
   int  fMtQueueEmptyUSleepTime = 0; // u seconds

public: // globals
   static bool gfMultithread;
   static int  gfMtMaxBacklog;
   static std::mutex gfLock; // Lock for modules to execute code that is not thread safe (many root fitting libraries)

public:
   TAMultithreadHelper(int nModules); // ctor
   ~TAMultithreadHelper(); // dtor
};

// flag that this version of manalyzer implements the profiler

#define HAVE_MANALYZER_PROFILER 1

// virtualized clock

#include <chrono>
typedef std::chrono::high_resolution_clock::time_point TAClock;
typedef std::chrono::duration<double> TAClockDuration;
inline TAClock TAClockNow() { return std::chrono::high_resolution_clock::now(); }

// user-controlled profiler

class TAUserProfilerFlow: public TAFlowEvent
{
public:
   TAClock fStart;
   TAClock fStop;

public:
   const std::string fModuleName;

public:
   TAUserProfilerFlow(TAFlowEvent* flow, const char* name, const TAClock& start); // ctor
   ~TAUserProfilerFlow(); // dtor
   double GetTimer() const;
};

// main program

int manalyzer_main(int argc, char* argv[]);

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

