#
# Makefile for the MIDAS analyzer component of ROOTANA
#
# Options:
# make NO_ROOT=1 # build without ROOT support
# make NO_MIDAS=1 # build without MIDAS support
#
# Build configurations:
# - as part of midas: should have ../include/midas.h
# - with midas at $MIDASSYS: should have $(MIDASSYS)/include/midas.h
# - standalone: packages mxml, mjson, mvodb, midasio must be cloned next to manalyzer (should have ../mjson/mjson.h, etc)
#

CXXFLAGS += -std=c++11 -g -O2 -Wall -Wuninitialized

# check for ROOT

ifdef NO_ROOT
ROOTSYS:=
else
ifneq ($(ROOTSYS),)
HAVE_ROOT:=1
endif
endif

# check for MIDAS

ifdef NO_MIDAS
MIDASSYS:=
else
ifneq (,$(wildcard ../include/midas.h))
MIDASSYS:=..
HAVE_MIDAS:=1
else
ifneq (,$(wildcard $(MIDASSYS)/include/midas.h))
HAVE_MIDAS:=1
endif
endif
endif


# find ROOT

ifdef HAVE_ROOT
ROOTCFLAGS := $(shell root-config --cflags)
ROOTFEATURES := $(shell root-config --features)
ROOTLIBDIR := $(shell root-config --libdir)
ROOTLIBS   := -L$(ROOTLIBDIR) $(shell root-config --libs) -lThread -ltbb
ROOTGLIBS  := -L$(ROOTLIBDIR) $(shell root-config --glibs) -lThread -ltbb
#ROOTRPATH  := -Wl,-rpath,$(ROOTLIBDIR) ### ROOT uses symlinks and RPATH cannot find them, use LD_LIBRARY_PATH instead. K.O. Jul 2021

CXXFLAGS += -DHAVE_ROOT
CXXFLAGS += $(ROOTCFLAGS)

HAVE_ROOT_HTTP := $(findstring http,$(ROOTFEATURES))

ifdef HAVE_ROOT_HTTP
CXXFLAGS += -DHAVE_THTTP_SERVER
ROOTLIBS += -lRHTTP
endif

LIBS += $(ROOTGLIBS)
LIBS += $(ROOTLIBS)
LIBS += $(ROOTRPATH)

endif

ifdef HAVE_MIDAS

# build with MIDAS

CXXFLAGS += -DHAVE_MIDAS -I$(MIDASSYS)/include -I$(MIDASSYS)/midasio -I$(MIDASSYS)/mvodb

# check that we have tmfe.h that supports TMEventBuffer

ifneq (,$(wildcard $(MIDASSYS)/include/tmfe.h))
ifneq (,$(shell grep TMEventBuffer $(MIDASSYS)/include/tmfe.h))
CXXFLAGS += -DHAVE_TMFE
endif
endif

LIBS += -L$(MIDASSYS)/lib -lmidas

else

# standalone build

CXXFLAGS += -I../midasio -I../mvodb -I../mxml -I../mjson

OBJS += midasio.o lz4frame.o xxhash.o lz4.o lz4hc.o
OBJS += mvodb.o nullodb.o mxmlodb.o mjsonodb.o mjson.o mxml.o

%.o: ../midasio/%.cxx
	$(CXX) -o $@ $(CXXFLAGS) -c $<

%.o: ../midasio/%.c
	$(CXX) -o $@ $(CXXFLAGS) -c $<

%.o: ../mvodb/%.cxx
	$(CXX) -o $@ $(CXXFLAGS) -c $<

%.o: ../mjson/%.cxx
	$(CXX) -o $@ $(CXXFLAGS) -c $<

%.o: ../mxml/%.cxx
	$(CXX) -o $@ $(CXXFLAGS) -c $<

endif

# select the main program - local custom main()
# or standard main() from rootana

MAIN := manalyzer_main.o

# build against a local copy of manalyzer.o

MANA := manalyzer.o

# uncomment and define analyzer modules here

#MODULES += agbars_module.o
#ALL     += barsana.exe

# analyzer with just the event dump module

ALL += manalyzer_test.exe

# examples

EXAMPLE_ALL += manalyzer_example_cxx.exe
EXAMPLE_ALL += manalyzer_example_flow.exe
EXAMPLE_ALL += manalyzer_example_flow_queue.exe
ifdef HAVE_ROOT
EXAMPLE_ALL += manalyzer_example_root.exe
EXAMPLE_ALL += manalyzer_example_root_graphics.exe
endif

all:: $(EXAMPLE_ALL)
all:: $(MODULES)
all:: $(ALL)

CMAKE := cmake

cmake: $(GIT_SUBMODULES)
	mkdir -p build
	cd build; $(CMAKE) ..; $(MAKE); $(MAKE) install

cclean:
	-rm -rf build bin lib include

test:
	./bin/manalyzer_test.exe --demo
	./bin/manalyzer_example_cxx.exe --demo
	./bin/manalyzer_example_flow.exe --demo
	./bin/manalyzer_example_flow_queue.exe --demo
	./bin/manalyzer_example_root.exe --demo
	./bin/manalyzer_example_root_graphics.exe --demo

%.exe: %.o manalyzer_main.o $(OBJS)
	$(CXX) -o $@ manalyzer_main.o $< $(CXXFLAGS) $(OBJS) $(LIBS) -lm -lz -lpthread

$(EXAMPLE_ALL): %.exe: %.o $(MAIN) $(MANA) $(OBJS)
	$(CXX) -o $@ $(MAIN) $(MANA) $< $(CXXFLAGS) $(OBJS) $(LIBS) -lm -lz -lpthread

%.exe: $(MAIN) $(MANA) $(MODULES) $(OBJS)
	$(CXX) -o $@ $(MAIN) $(MANA) $(MODULES) $(CXXFLAGS) $(OBJS) $(LIBS) -lm -lz -lpthread

%.o: %.cxx
	$(CXX) -o $@ $(CXXFLAGS) -c $<

html/index.html:
	-mkdir html
	-make -k dox
	touch html/index.html

dox:
	doxygen

clean::
	-rm -f *.o *.a *.exe

clean::
	-rm -f $(ALL)

clean::
	-rm -f $(EXAMPLE_ALL)

clean::
	-rm -rf *.exe.dSYM

clean::
	-rm -rf html

# end
