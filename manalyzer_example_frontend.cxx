//
// MIDAS analyzer example: MIDAS frontend for sending analyzed data back into MIDAS
//
// K.Olchanski
//

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <cassert>

#include "manalyzer.h"
#include "midasio.h"

class TMEventFlow : public TAFlowEvent
{
public:
   TMEvent fEvent;

   TMEventFlow(TAFlowEvent* flow, const TMEvent& event)
      : TAFlowEvent(flow)
   {
      fEvent = event;
   }
};

/*******************************************************************\

  Name:         tmfe_example_everything.cxx
  Created by:   K.Olchanski

  Contents:     Example Front end to demonstrate all functions of TMFE class

\********************************************************************/


#include <stdio.h>
#include "tmfe.h"

class EqAnalyzer :
   public TMFeEquipment
{
public:
   EqAnalyzer(const char* eqname, const char* eqfilename) // ctor
      : TMFeEquipment(eqname, eqfilename)
   {
      printf("EqAnalyzer::ctor!\n");

      // configure the equipment here:
      
      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 100;
      fEqConfPeriodMilliSec = 0;
      //fEqConfWriteEventsToOdb = true;
      fEqConfBuffer = "ANAA";
   }

   ~EqAnalyzer() // dtor
   {
      printf("EqAnalyzer::dtor!\n");
   }

   void HandleUsage()
   {
      printf("EqAnalyzer::HandleUsage!\n");
   }

   TMFeResult HandleInit(const std::vector<std::string>& args)
   {
      printf("EqAnalyzer::HandleInit!\n");
      fEqConfReadOnlyWhenRunning = false; // overwrite ODB Common RO_RUNNING to false
      //fEqConfWriteEventsToOdb = true; // overwrite ODB Common RO_ODB to true
      //EqSetStatus("Started...", "white");
      return TMFeOk();
   }
};

// example frontend

class FeAnalyzer: public TMFrontend
{
public:
};

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

class ExampleFrontend: public TARunObject
{
public:
   EqAnalyzer* fEq = NULL;

public:
   ExampleFrontend(TARunInfo* runinfo, EqAnalyzer* eq)
      : TARunObject(runinfo)
   {
      printf("ExampleFrontend::ctor, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      fModuleName = "ExampleFrontend";
      fEq = eq;
   }

   ~ExampleFrontend()
   {
      printf("ExampleFrontend::dtor!\n");
   }

   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
   {
      printf("ExampleFrontend::Analyze, run %d, event serno %d, id 0x%04x, data size %d\n", runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);

      flow = new TMEventFlow(flow, *event);

      return flow;
   }
      
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      printf("ExampleFrontend::AnalyzeFlowEvent, run %d\n", runinfo->fRunNo);

      if (!flow)
         return flow;

      TMEventFlow* ef = flow->Find<TMEventFlow>();

      if (!ef)
         return flow;

      uint32_t anaa_bank[] = { 1, 2, 3, 4 };

      ef->fEvent.AddBank("ANAA", TID_UINT32, (const char*)&anaa_bank[0], sizeof(anaa_bank));

      ef->fEvent.PrintHeader();

      if (fEq) {
         fEq->EqSendEvent(ef->fEvent.data);
         fEq->EqWriteStatistics();
      }

      return flow;
   }
};

class ExampleFrontendFactory: public TAFactory
{
   FeAnalyzer* fFe = NULL;
   EqAnalyzer* fEq = NULL;

   void Init(const std::vector<std::string> &args)
   {
      printf("ExampleFrontendFactory::Init!\n");

      assert(fEq == NULL);

      fFe = new FeAnalyzer();
      fEq = new EqAnalyzer("analyzer", __FILE__);
      fFe->FeAddEquipment(fEq);
      
      TMFeResult r = fFe->FeInitEquipments(args);
      
      if (r.error_flag) {
         fprintf(stderr, "Cannot initialize equipments, error message: %s, bye.\n", r.error_message.c_str());
         fFe->fMfe->Disconnect();
         exit(1);
      }
   }

   void Finish()
   {
      printf("ExampleFrontendFactory::Finish!\n");

      //if (fEq) {
      //   delete fEq;
      //   fEq = NULL;
      //}
      if (fFe) {
         delete fFe;
         fFe = NULL;
      }
   }
  
   ExampleFrontend* NewRunObject(TARunInfo* runinfo)
   {
      return new ExampleFrontend(runinfo, fEq);
   }
};

static TARegister tar(new ExampleFrontendFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
